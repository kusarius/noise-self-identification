### Building and testing:
- Command to build "nsi" (CLI): `go build -o ..\dist\nsi\nsi.exe -tags forceposix .\cmd` (the `forceposix` build tag will disable parsing of Windows-style command line arguments, like `/v` or `/verbose`)
- Command to build "gnsi" (GUI): `go build -o ..\dist\gnsi\bin\gnsi.exe -ldflags -H=windowsgui .\gui` (the `windowsgui` flag will hide the terminal window when starting the app)
- Command to run all tests: `go test ./...` (you can add `-v` for additional info)

### Notes:
- Record noise only from stereo mixer
- When recording noise, disable all other audio devices like microphones and line-in inputs
- 16-bit and 24-bit sound cards store samples **differently**, so we have to check the bit depth of selected device before capturing
- Very handy one-liner to count the lines of code: `git ls-files | grep '\.go' | xargs wc -l`
- How to distribute GTK applications: [link](https://stackoverflow.com/questions/49092784/how-to-distribute-a-gtk-application-on-windows)

### To Do:
- [x] Implement a module that will enumerate all supported formats of selected audio device ([useful link](https://stackoverflow.com/questions/50396224/how-to-get-audio-formats-supported-by-physical-device-winapi-windows))
- [x] Implement a module that will disable Windows system sounds ([useful link](https://stackoverflow.com/questions/32866703/visual-c-mfc-disable-windows-sounds))
- [x] Implement a module that will set system master volume to 0
- [x] Refactor all Go code to use **packages** instead of just *"main"* package with a lot of files
- [x] Rewrite **capturing** and **bittemplate** packets so that they don't depend on config file (they must depend only on function arguments). This will improve testability and flexibility
- [x] Improve *noise capturing* and *bit template generation* tests
- [ ] Try to statically link PortAudio to the binary (will it work crossplatform?)
- [x] Improve command line arguments parsing (configurable config path, named arguments instead of positioned ones, etc)
- [ ] Test the program on at least **10** different computers (both Linux and Windows), collect statistics