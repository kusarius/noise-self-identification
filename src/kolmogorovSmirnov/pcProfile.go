package kolmogorovSmirnov

import "fmt"

type PCProfile struct {
	BitTemplates   [][]float64 `json:"bit-templates"`
	DistanceMatrix [][]float64 `json:"distance-matrix"`
}

func (pp *PCProfile) PrettyPrint() {
	fmt.Println("Templates:")
	for _, t := range pp.BitTemplates {
		tStr := ""
		for _, el := range t {
			if el == 1.0 {
				tStr += "1"
			} else {
				tStr += "0"
			}
		}

		fmt.Printf("%s\n\n", tStr)
	}

	fmt.Println("Matrix:")
	for i := 0; i < len(pp.DistanceMatrix); i++ {
		for c := 0; c < len(pp.DistanceMatrix); c++ {
			fmt.Printf("%10.0f ", pp.DistanceMatrix[c][i])
		}
		fmt.Println()
	}
}
