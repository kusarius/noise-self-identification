package kolmogorovSmirnov

import (
	gomath "math"

	"gitlab.com/kusarius/noise-self-identification/math"
)

func variance(E []float64) float64 {
	sum := 0.0
	meanE := math.Mean(E)

	for _, ei := range E {
		sum += (ei - meanE) * (ei - meanE)
	}

	return sum / float64(len(E))
}

func normalizedAutocorrelation(E []float64) []float64 {
	res := make([]float64, len(E))
	meanE, varE := math.Mean(E), variance(E)

	for n := 0; n < len(E); n++ {
		sum := 0.0
		for i := 0; i < len(E)-n; i++ {
			sum += (E[n+i] - meanE) * (E[i] - meanE)
		}

		res[n] = sum / (varE * (float64(len(E) + 1)))
	}

	return res
}

func HammingDistance(Bl, Bm []float64) float64 {
	res := 0.0
	for n := 0; n < len(Bl); n++ {
		res += gomath.Abs(Bl[n] - Bm[n])
	}
	return res
}

func bitTemplate(E []float64) []float64 {
	res := make([]float64, len(E)-1)
	a := normalizedAutocorrelation(E)

	for m := 0; m < len(E)-1; m++ {
		if a[m+1] >= a[m] {
			res[m] = 1.0
		} else {
			res[m] = 0.0
		}
	}

	return res
}

func distanceMatrix(B [][]float64) [][]float64 {
	res := make([][]float64, len(B))
	for l := 0; l < len(B); l++ {
		res[l] = make([]float64, len(B))
		for m := 0; m < len(B); m++ {
			res[l][m] = HammingDistance(B[l], B[m])
		}
	}

	return res
}

func EmpiricalDistribution(H [][]float64) []float64 {
	maxH := -1
	totalElems := len(H) * len(H)
	freq := make(map[int]float64)

	for i := 0; i < len(H); i++ {
		for c := 0; c < len(H); c++ {
			el := int(H[i][c])

			if el > maxH {
				maxH = el
			}

			if _, ok := freq[el]; !ok {
				freq[el] = 1
			} else {
				freq[el]++
			}
		}
	}

	res := make([]float64, maxH+1)
	for num, fr := range freq {
		res[num] = fr / float64(totalElems)
	}

	for i := 1; i < len(res); i++ {
		res[i] += res[i-1]
	}

	return res
}
