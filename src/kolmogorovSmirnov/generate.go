package kolmogorovSmirnov

import (
	"github.com/pkg/errors"

	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/capturing/raw"
)

func GeneratePCProfile(capturingParams capturing.NoiseCapturingParams, numberOfCaptures, requiredLength int) (*PCProfile, error) {
	capturer := new(raw.NoiseCapturer)
	bitTemplates := make([][]float64, numberOfCaptures)

	for i := 0; i < numberOfCaptures; i++ {
		noise, err := capturer.CaptureNoise(capturingParams, requiredLength)
		if err != nil {
			return nil, errors.Wrap(err, "unable to generate PC profile")
		}

		bitTemplates[i] = bitTemplate(noise)
	}

	distanceMatrix := distanceMatrix(bitTemplates)

	return &PCProfile{bitTemplates, distanceMatrix}, nil
}
