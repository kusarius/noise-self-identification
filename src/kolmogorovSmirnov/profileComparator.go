package kolmogorovSmirnov

type ProfileComparator interface {
	// Returns true if profiles are similar and authentication is successful
	CompareProfiles(pp1, pp2 *PCProfile) bool
}
