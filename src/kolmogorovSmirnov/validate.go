package kolmogorovSmirnov

import (
	"github.com/pkg/errors"
	"gitlab.com/kusarius/noise-self-identification/capturing"
)

func ValidatePC(
	capturingParams capturing.NoiseCapturingParams,
	referenceProfile *PCProfile,
	comparator ProfileComparator,
) (bool, error) {
	numberOfCaptures := len(referenceProfile.DistanceMatrix)
	requiredLength := len(referenceProfile.BitTemplates[0])

	profile, err := GeneratePCProfile(capturingParams, numberOfCaptures, requiredLength)
	if err != nil {
		return false, errors.Wrap(err, "unable to generate profile for validation")
	}

	return comparator.CompareProfiles(referenceProfile, profile), nil
}
