package profileComparator

import "gitlab.com/kusarius/noise-self-identification/kolmogorovSmirnov"

type Simple struct{}

func (pc *Simple) CompareProfiles(pp1, pp2 *kolmogorovSmirnov.PCProfile) bool {
	maxH1, maxH2 := -1.0, -1.0
	for i := 0; i < len(pp1.DistanceMatrix); i++ {
		for c := 0; c < len(pp1.DistanceMatrix); c++ {
			if pp1.DistanceMatrix[i][c] > maxH1 {
				maxH1 = pp1.DistanceMatrix[i][c]
			}

			if pp2.DistanceMatrix[i][c] > maxH2 {
				maxH2 = pp2.DistanceMatrix[i][c]
			}
		}
	}

	return maxH1 >= maxH2
}
