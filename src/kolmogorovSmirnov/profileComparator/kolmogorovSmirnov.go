package profileComparator

import (
	"math"

	"gitlab.com/kusarius/noise-self-identification/kolmogorovSmirnov"
)

type KolmogorovSmirnov struct {
	Alpha float64
}

func (pc *KolmogorovSmirnov) CompareProfiles(pp1, pp2 *kolmogorovSmirnov.PCProfile) bool {
	F1 := kolmogorovSmirnov.EmpiricalDistribution(pp1.DistanceMatrix)
	F2 := kolmogorovSmirnov.EmpiricalDistribution(pp2.DistanceMatrix)

	minLen := len(F1)
	if len(F1) > len(F2) {
		minLen = len(F2)
	}

	maxDistance := -1.0
	for i := 0; i < minLen; i++ {
		distance := math.Abs(F1[i] - F2[i])
		if distance > maxDistance {
			maxDistance = distance
		}
	}

	n, m := 100.0, 100.0
	lambdaEmpirical := math.Sqrt((n*m)/(n+m)) * maxDistance

	var lambdaCritical float64
	switch pc.Alpha {
	case 0.01:
		lambdaCritical = 1.95
	case 0.1:
		lambdaCritical = 1.22
	case 0.5:
		lambdaCritical = 1.36
	default:
		return false
	}

	return lambdaEmpirical < lambdaCritical
}
