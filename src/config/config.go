package config

import (
	"encoding/json"
	"os"
)

type Config struct {
	Debug struct {
		SaveAutocorrelation         bool   `json:"saveAutocorrelation"`
		SaveAutocorrelationFileName string `json:"saveAutocorrelationFileName"`

		SaveNoise         bool   `json:"saveNoise"`
		SaveNoiseFileName string `json:"saveNoiseFileName"`
	} `json:"debug"`

	Capturing struct {
		FramesPerBuffer int   `json:"framesPerBuffer"`
		SampleRate      int   `json:"sampleRate"`
		Offset          int   `json:"offset"`
		NoiseBits       []int `json:"noiseBits"`
	} `json:"capturing"`

	NumberOfCaptures  int `json:"numberOfCaptures"`
	BitTemplateLength int `json:"bitTemplateLength"`
}

func loadDefaults(config *Config) {
	config.Debug.SaveAutocorrelation = false
	config.Debug.SaveNoise = false

	config.Capturing.FramesPerBuffer = 256
	config.Capturing.SampleRate = 44100
	config.Capturing.Offset = 500
	config.Capturing.NoiseBits = []int{16}

	config.NumberOfCaptures = 10
	config.BitTemplateLength = 1000
}

func loadConfig(fileName string) *Config {
	config := new(Config)

	configFile, err := os.Open(fileName)
	defer configFile.Close()

	if err != nil {
		loadDefaults(config)
	} else if json.NewDecoder(configFile).Decode(&config) != nil {
		loadDefaults(config)
	}

	return config
}

var configInstance *Config

// Default config file name.
var configFileName = "config.json"

func Init(fileName string) {
	configFileName = fileName
}

func GetInstance() *Config {
	if configInstance == nil {
		configInstance = loadConfig(configFileName)
	}

	return configInstance
}
