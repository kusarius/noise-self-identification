package main

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"time"

	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"strconv"

	"gitlab.com/kusarius/noise-self-identification/bittemplate"

	"github.com/gen2brain/beeep"
)

func onValidateButtonClicked() {
	if selectedDevice < 0 || selectedDevice >= len(inputDevices) {
		return
	}

	inputTemplate, err := openTemplateFromFile()
	if err != nil {
		return
	}

	// To avoid double clicking.
	controlPanel.SetProperty("sensitive", false)
	treeViewDevices.SetProperty("sensitive", false)

	go func() {
		template := captureTemplate(1)

		confidence, err := bittemplate.CompareTemplates(inputTemplate, template, config.BitTemplateLength)
		handleError(err)

		// As we cannot create GUI components in a separate goroutine,
		// we use IdleAdd to schedule the creation of message dialog
		// to the time when the application is not busy.
		// This will be called from main goroutine.
		glib.IdleAdd(func() bool {
			messageBoxInfo("Validation", "Confidence: %.2f%%", confidence)

			controlPanel.SetProperty("sensitive", true)
			treeViewDevices.SetProperty("sensitive", true)

			// If this callback returns false, it will be called only once
			// and then automatically removed from the calling queue.
			return false
		})
	}()
}

func onGenerateButtonClicked() {
	if selectedDevice < 0 || selectedDevice >= len(inputDevices) {
		return
	}

	// To avoid double clicking.
	controlPanel.SetProperty("sensitive", false)
	treeViewDevices.SetProperty("sensitive", false)

	go func() {
		template := captureTemplate(config.NumberOfCaptures)

		dialog, err := gtk.FileChooserNativeDialogNew(
			"Select file",
			window,
			gtk.FILE_CHOOSER_ACTION_SAVE,
			"Save",
			"Cancel",
		)

		handleError(err)

		dialog.SetDoOverwriteConfirmation(true)
		addFilterToDialog(dialog, "All files", "*.*")

		if dialog.Run() == int(gtk.RESPONSE_ACCEPT) {
			handleError(ioutil.WriteFile(dialog.GetFilename(), []byte(template), 0644))
		}

		glib.IdleAdd(func() bool {
			controlPanel.SetProperty("sensitive", true)
			treeViewDevices.SetProperty("sensitive", true)
			return false
		})
	}()
}

func onDeviceListSelect(selection *gtk.TreeSelection) {
	model, iter, ok := selection.GetSelected()
	if ok {
		tpath, err := model.(*gtk.TreeModel).GetPath(iter)
		handleError(err)

		selectedDevice, err = strconv.Atoi(tpath.String())
		handleError(err)

		// We can enable buttons when we selected something.
		controlPanel.SetProperty("sensitive", true)
		buttonStopMonitor.SetProperty("sensitive", false)
	}
}

func onMonitorStartButtonClicked() {
	if selectedDevice < 0 || selectedDevice >= len(inputDevices) {
		return
	}

	inputTemplate, err := openTemplateFromFile()
	if err != nil {
		return
	}

	controlPanel.SetProperty("sensitive", false)
	treeViewDevices.SetProperty("sensitive", false)
	buttonStopMonitor.SetProperty("sensitive", true)

	stopMonitoringChan = make(chan bool)

	go func() {
		logFile, err := os.OpenFile("monitoring.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		handleError(err)
		monLogger := log.New(logFile, "", log.LstdFlags)
		defer logFile.Close()

		if !switchMonLogging.GetActive() {
			monLogger.SetOutput(ioutil.Discard)
		}

		monLogger.Printf(
			"Started monitoring with period of %d minutes and threshold of %d%%\n",
			spinMonitorPeriod.GetValueAsInt(),
			spinAlertThreshold.GetValueAsInt(),
		)

		// Used to calculate the average confidence.
		confSum, monCount := 0.0, 0.0

	loop:
		for {
			template := captureTemplate(1)

			confidence, err := bittemplate.CompareTemplates(inputTemplate, template, config.BitTemplateLength)
			handleError(err)

			confSum += confidence
			monCount += 1

			monLogger.Printf("Current confidence: %.2f%%; average: %.2f%%\n", confidence, confSum/monCount)

			if math.Abs(confidence-confSum/monCount) > spinAlertThreshold.GetValue() {
				monLogger.Println("Alert! Difference between current confidence and average is larger than threshold")
				beeep.Notify(
					"Noise Self-Identification",
					fmt.Sprintf("Something's wrong: confidence is %.2f%%, but average is %.2f%%", confidence, confSum/monCount),
					"../share/assets/warning.png",
				)
			}

			select {
			case <-stopMonitoringChan:
				break loop
			case <-time.After(time.Duration(spinMonitorPeriod.GetValueAsInt()) * time.Minute):
				// Nothing to do.
			}
		}

		monLogger.Println("Stopped monitoring")

		glib.IdleAdd(func() bool {
			controlPanel.SetProperty("sensitive", true)
			treeViewDevices.SetProperty("sensitive", true)
			buttonStopMonitor.SetProperty("sensitive", false)
			return false
		})
	}()
}

func onMonitorStopButtonClicked() {
	stopMonitoringChan <- true
}
