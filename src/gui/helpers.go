package main

import (
	"errors"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/kusarius/noise-self-identification/audiocontrol"
	"gitlab.com/kusarius/noise-self-identification/bittemplate"
	"gitlab.com/kusarius/noise-self-identification/capturing"
)

func messageBoxInfo(title, format string, args ...interface{}) {
	messageDialog := gtk.MessageDialogNew(
		window,
		gtk.DIALOG_MODAL,
		gtk.MESSAGE_INFO,
		gtk.BUTTONS_OK,
		format, args...,
	)

	messageDialog.Connect("response", messageDialog.Destroy)
	messageDialog.SetTitle(title)
	messageDialog.Run()
}

func handleError(err error) {
	if err != nil {
		log.Println(err.Error())

		glib.IdleAdd(func() bool {
			messageBoxInfo("Error", err.Error())
			os.Exit(1)

			return false
		})

		// In case the error occured during capturing - unmute the device.
		muted, _ := audiocontrol.IsMuted()
		if muted {
			audiocontrol.Unmute()
		}

		// Wait until user closes the message box
		// (anyway, after that we will quit the program).
		select {}
	}
}

func addFilterToDialog(dialog *gtk.FileChooserNativeDialog, name, pattern string) {
	fileFilter, err := gtk.FileFilterNew()
	handleError(err)

	fileFilter.SetName(name)
	fileFilter.AddPattern(pattern)
	dialog.AddFilter(fileFilter)
}

func openTemplateFromFile() (string, error) {
	dialog, err := gtk.FileChooserNativeDialogNew(
		"Select file",
		window,
		gtk.FILE_CHOOSER_ACTION_OPEN,
		"Open",
		"Cancel",
	)

	handleError(err)

	dialog.SetDoOverwriteConfirmation(true)
	addFilterToDialog(dialog, "Template files", "*.tpl")
	addFilterToDialog(dialog, "All files", "*.*")

	if dialog.Run() == int(gtk.RESPONSE_ACCEPT) {
		templateBytes, err := ioutil.ReadFile(dialog.GetFilename())
		handleError(err)

		inputTemplate := strings.TrimSpace(string(templateBytes))
		if bittemplate.IsValidTemplate(inputTemplate, config.BitTemplateLength) {
			return inputTemplate, nil
		} else {
			log.Println("Invalid template")
		}
	}

	return "", errors.New("No valid template")
}

func captureTemplate(numberOfCaptures int) string {
	handleError(audiocontrol.SafeMuteUnmute(true))

	time.Sleep(1 * time.Second)

	capturingParams := capturing.NoiseCapturingParams{
		InputDevice:     inputDevices[selectedDevice],
		SampleRate:      config.Capturing.SampleRate,
		FramesPerBuffer: config.Capturing.FramesPerBuffer,
		CapturingOffset: config.Capturing.Offset,
		NoiseBits:       config.Capturing.NoiseBits,
	}

	newBitTemplate, err := bittemplate.GenerateBitTemplate(capturingParams, numberOfCaptures, config.BitTemplateLength)
	handleError(err)

	handleError(audiocontrol.SafeMuteUnmute(false))

	return newBitTemplate
}
