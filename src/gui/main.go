package main

import (
	"github.com/gordonklaus/portaudio"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"

	"log"
	"os"
	"runtime"

	cfg "gitlab.com/kusarius/noise-self-identification/config"
	"gitlab.com/kusarius/noise-self-identification/devices"
)

// Capturing related stuff.
var (
	inputDevices   []*portaudio.DeviceInfo
	selectedDevice = -1

	config *cfg.Config
)

// GUI related stuff.
var (
	// Mapping of signals configured in Glade to actual callbacks.
	signals = map[string]interface{}{
		"onMainWindowDestroy":         func() { gtk.MainQuit() },
		"onGenerateButtonClicked":     onGenerateButtonClicked,
		"onDeviceListSelect":          onDeviceListSelect,
		"onValidateButtonClicked":     onValidateButtonClicked,
		"onMonitorStartButtonClicked": onMonitorStartButtonClicked,
		"onMonitorStopButtonClicked":  onMonitorStopButtonClicked,
	}

	// What GTK theme to use on each OS.
	osThemeMapping = map[string]string{
		"windows": "adawita",
		"linux":   "adawita",
	}

	stopMonitoringChan chan bool

	// GUI controls.
	builder            *gtk.Builder
	window             *gtk.Window
	devicesListstore   *gtk.ListStore
	controlPanel       *gtk.Fixed
	buttonStopMonitor  *gtk.Button
	spinMonitorPeriod  *gtk.SpinButton
	spinAlertThreshold *gtk.SpinButton
	treeViewDevices    *gtk.TreeView
	switchMonLogging   *gtk.Switch
)

func configureWindow() {
	var ok bool

	obj, err := builder.GetObject("mainWindow")
	handleError(err)

	window, ok = obj.(*gtk.Window)
	if !ok {
		log.Fatal("Not a *gtk.Window")
	}

	settings, err := gtk.SettingsGetDefault()
	handleError(err)
	handleError(settings.SetProperty("gtk_theme_name", osThemeMapping[runtime.GOOS]))
}

func loadInputDevices() {
	var err error
	inputDevices, err = devices.GetInputDevices()
	handleError(err)

	for _, dev := range inputDevices {
		iter := devicesListstore.Append()
		devicesListstore.SetValue(iter, 0, dev.Name)
		devicesListstore.SetValue(iter, 1, dev.HostApi.Type.String())
	}
}

func loadGUIControls() {
	var ok bool

	obj, err := builder.GetObject("buttonStopMonitor")
	handleError(err)
	buttonStopMonitor, ok = obj.(*gtk.Button)
	if !ok {
		log.Fatal("Not a *gtk.Button")
	}

	obj, err = builder.GetObject("spinMonitorPeriod")
	handleError(err)
	spinMonitorPeriod, ok = obj.(*gtk.SpinButton)
	if !ok {
		log.Fatal("Not a *gtk.SpinButton")
	}

	obj, err = builder.GetObject("spinAlertThreshold")
	handleError(err)
	spinAlertThreshold, ok = obj.(*gtk.SpinButton)
	if !ok {
		log.Fatal("Not a *gtk.SpinButton")
	}

	obj, err = builder.GetObject("controlPanel")
	handleError(err)
	controlPanel, ok = obj.(*gtk.Fixed)
	if !ok {
		log.Fatal("Not a *gtk.Fixed")
	}

	obj, err = builder.GetObject("treeViewDevices")
	handleError(err)
	treeViewDevices, ok = obj.(*gtk.TreeView)
	if !ok {
		log.Fatal("Not a *gtk.TreeView")
	}

	obj, err = builder.GetObject("switchMonLogging")
	handleError(err)
	switchMonLogging, ok = obj.(*gtk.Switch)
	if !ok {
		log.Fatal("Not a *gtk.Switch")
	}

	obj, err = builder.GetObject("devicesListstore")
	handleError(err)
	devicesListstore, ok = obj.(*gtk.ListStore)
	if !ok {
		log.Fatal("Not a *gtk.ListStore")
	}
}

func main() {
	portaudio.Initialize()
	defer portaudio.Terminate()

	cfg.Init("../config.json")
	config = cfg.GetInstance()

	application, err := gtk.ApplicationNew("com.kusarius.gnsi", glib.APPLICATION_FLAGS_NONE)
	handleError(err)

	application.Connect("activate", func() {
		builder, err = gtk.BuilderNewFromFile("../ui/mainWindow.glade")
		handleError(err)

		builder.ConnectSignals(signals)

		configureWindow()
		loadGUIControls()
		loadInputDevices()

		window.Show()
		application.AddWindow(window)
	})

	application.Run(os.Args)
}
