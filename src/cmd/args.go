package main

import (
	"github.com/jessevdk/go-flags"

	"fmt"
	"os"
	"strings"
)

type CommandLineArgs struct {
	Action            string  `short:"a" long:"action" description:"Action to perform (generate/validate)" required:"true"`
	TemplateFileName  string  `short:"t" long:"template" description:"Path to the template file to save/validate" required:"true"`
	ConfigFileName    string  `short:"c" long:"config" description:"Path to the config file" default:"config.json"`
	KolmogorovSmirnov bool    `short:"k" long:"kolmogorov-smirnov" description:"Use Kolmogorov-Smirnov algorithm to generate and validate bit templates"`
	ProfileComparator string  `short:"p" long:"profile-comparator" description:"Profile comparator to use (simple/kolmogorov-smirnov)" default:"kolmogorov-smirnov"`
	ComparatorAlpha   float64 `short:"l" long:"comparator-alpha" description:"Alpha parameter of Kolmogorov-Smirnov algorithm (importance level)" default:"0.1"`
}

// Checks if given file exists.
func fileExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}

	return true
}

func exitWithMessage(msg string) {
	fmt.Println(msg)
	os.Exit(0)
}

// Returns the given string with the first character in upper case.
func capitalizeString(str string) string {
	return strings.ToUpper(string(str[0])) + str[1:]
}

// Parses and validates command line arguments.
func parseArgs() CommandLineArgs {
	cmdArgs := CommandLineArgs{}
	parser := flags.NewParser(&cmdArgs, flags.HelpFlag)

	_, err := parser.Parse()
	if err != nil {
		// Flags library returns all error messages in lowercase, so capitalize it.
		exitWithMessage(capitalizeString(err.Error()))
	}

	if cmdArgs.Action != "generate" && cmdArgs.Action != "validate" {
		exitWithMessage("Invalid action!")
	}

	if cmdArgs.Action == "validate" && !fileExists(cmdArgs.TemplateFileName) {
		exitWithMessage("Path to the template you specified doesn't exist!")
	}

	if !fileExists(cmdArgs.ConfigFileName) {
		exitWithMessage("Path to the config file you specified doesn't exist!")
	}

	if cmdArgs.ProfileComparator != "simple" && cmdArgs.ProfileComparator != "kolmogorov-smirnov" {
		exitWithMessage("Invalid profile comparator. Valid comparators are 'simple' and 'kolmogorov-smirnov'.")
	}

	if cmdArgs.ComparatorAlpha != 0.01 && cmdArgs.ComparatorAlpha != 0.1 && cmdArgs.ComparatorAlpha != 0.5 {
		exitWithMessage("Invalid comparator alpha parameter. Valid values are 0.01, 0.1 and 0.5.")
	}

	return cmdArgs
}
