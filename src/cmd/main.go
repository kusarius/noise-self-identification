package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/kolmogorovSmirnov"
	"gitlab.com/kusarius/noise-self-identification/kolmogorovSmirnov/profileComparator"

	"github.com/gordonklaus/portaudio"

	"gitlab.com/kusarius/noise-self-identification/audiocontrol"
	"gitlab.com/kusarius/noise-self-identification/bittemplate"
	cfg "gitlab.com/kusarius/noise-self-identification/config"
	"gitlab.com/kusarius/noise-self-identification/devices"
)

// Prints the main information about the device to the terminal.
func printDeviceInfo(device *portaudio.DeviceInfo) {
	fmt.Printf("\tHost API: %s\n", device.HostApi.Type.String())
	fmt.Printf("\tDefault sample rate: %f\n", device.DefaultSampleRate)
	fmt.Printf("\tMax input channels: %d\n", device.MaxInputChannels)
	fmt.Printf("\tLow input latency: %f s\n", device.DefaultLowInputLatency.Seconds())
	fmt.Printf("\tHigh input latency: %f s\n\n", device.DefaultHighInputLatency.Seconds())
}

func checkForError(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(0)
	}
}

func chooseInputDevice() *portaudio.DeviceInfo {
	inputDevices, err := devices.GetInputDevices()
	checkForError(err)

	if len(inputDevices) == 0 {
		fmt.Println("You don't have any available input devices!")
		os.Exit(0)
	}

	fmt.Println("List of available input devices: ")
	for ind, idv := range inputDevices {
		fmt.Printf("%d)\tName: %s\n", ind+1, idv.Name)
		printDeviceInfo(idv)
	}

	choice := 0
	for choice < 1 || choice > len(inputDevices) {
		fmt.Print("Choose the input device: ")
		fmt.Scanf("%d\n", &choice)
	}

	return inputDevices[choice-1]
}

func oldAlgorithm(
	capturingParams capturing.NoiseCapturingParams,
	numberOfCaptures int,
	config *cfg.Config,
	cmdArgs CommandLineArgs,
) {
	newBitTemplate, err := bittemplate.GenerateBitTemplate(capturingParams, numberOfCaptures, config.BitTemplateLength)
	checkForError(err)

	if cmdArgs.Action == "generate" {
		ioutil.WriteFile(cmdArgs.TemplateFileName, []byte(newBitTemplate), 0644)

		fmt.Printf("Average template based on %d captures:\n", numberOfCaptures)
		fmt.Println(newBitTemplate + "\n")
	} else {
		fmt.Println("Captured template:")
		fmt.Println(newBitTemplate + "\n")

		fileContentBytes, err := ioutil.ReadFile(cmdArgs.TemplateFileName)
		checkForError(err)

		inputTemplate := strings.TrimSpace(string(fileContentBytes))

		confidence, err := bittemplate.CompareTemplates(inputTemplate, newBitTemplate, config.BitTemplateLength)
		if err != nil {
			fmt.Println("File you specified does not contain valid bit template!")
			return
		}

		fmt.Printf("Confidence: %.2f%%\n", confidence)
	}
}

func main() {
	cmdArgs := parseArgs()

	cfg.Init(cmdArgs.ConfigFileName)
	config := cfg.GetInstance()

	portaudio.Initialize()
	defer portaudio.Terminate()

	inputDevice := chooseInputDevice()
	supportedFormats := devices.GetDeviceSupportedFormats(inputDevice)
	fmt.Println("Device you have chosen: " + inputDevice.Name + "\n")

	fmt.Print("Supported sample rates: ")
	for _, el := range supportedFormats.SampleRates {
		fmt.Print(el)
		fmt.Print(" ")
	}
	fmt.Print("\n")

	fmt.Print("Supported bit depths: ")
	for _, el := range supportedFormats.BitDepths {
		fmt.Print(el)
		fmt.Print(" ")
	}
	fmt.Print("\n\n")

	var numberOfCaptures int
	if cmdArgs.Action == "generate" {
		numberOfCaptures = config.NumberOfCaptures
	} else {
		numberOfCaptures = 1
	}

	checkForError(audiocontrol.SafeMuteUnmute(true))
	defer audiocontrol.SafeMuteUnmute(false)

	// We sleep for a second because even after muting the device some noise
	// is still coming out of mixer. So we wait a bit to skip that residual noise.
	time.Sleep(1 * time.Second)

	capturingParams := capturing.NoiseCapturingParams{
		InputDevice:     inputDevice,
		SampleRate:      config.Capturing.SampleRate,
		FramesPerBuffer: config.Capturing.FramesPerBuffer,
		CapturingOffset: config.Capturing.Offset,
		NoiseBits:       config.Capturing.NoiseBits,
	}

	if cmdArgs.KolmogorovSmirnov {
		if cmdArgs.Action == "generate" {
			pcProfile, err := kolmogorovSmirnov.GeneratePCProfile(capturingParams, numberOfCaptures, config.BitTemplateLength)
			checkForError(err)

			pcProfile.PrettyPrint()

			serializedProfile, err := json.Marshal(pcProfile)
			checkForError(err)

			fmt.Printf("\nSaving PC profile to %s\n", cmdArgs.TemplateFileName)
			ioutil.WriteFile(cmdArgs.TemplateFileName, serializedProfile, 0644)
		} else {
			fileContentBytes, err := ioutil.ReadFile(cmdArgs.TemplateFileName)
			checkForError(err)

			referenceProfile := new(kolmogorovSmirnov.PCProfile)
			err = json.Unmarshal(fileContentBytes, referenceProfile)
			checkForError(err)

			var comparator kolmogorovSmirnov.ProfileComparator
			switch cmdArgs.ProfileComparator {
			case "simple":
				comparator = new(profileComparator.Simple)
			case "kolmogorov-smirnov":
				comparator = &profileComparator.KolmogorovSmirnov{Alpha: cmdArgs.ComparatorAlpha}
			}

			authResult, err := kolmogorovSmirnov.ValidatePC(capturingParams, referenceProfile, comparator)
			checkForError(err)

			fmt.Printf("Authentication result: %v\n", authResult)
		}
	} else {
		oldAlgorithm(capturingParams, numberOfCaptures, config, cmdArgs)
	}
}
