package bittemplate

import (
	"fmt"
	"os"

	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/capturing/selective"
	"gitlab.com/kusarius/noise-self-identification/config"
	"gitlab.com/kusarius/noise-self-identification/math"
)

// Generates average bit template based on captured noise.
//
// Arguments:
// capturingParams:  Noise capturing parameters.
// numberOfCaptures: Total number of captures before calculating the average template.
// requiredLength:   Required length of average bit template.
func GenerateBitTemplate(capturingParams capturing.NoiseCapturingParams, numberOfCaptures, requiredLength int) (string, error) {
	// This method is intended to be only used with selective capturer
	capturer := new(selective.NoiseCapturer)

	if requiredLength == 0 {
		return "", nil
	}

	// Truncate the file before new capture session.
	if config.GetInstance().Debug.SaveNoise {
		os.Create(config.GetInstance().Debug.SaveNoiseFileName)
	}

	avg_autocorr := make([]float64, requiredLength)
	for i := 0; i < numberOfCaptures; i++ {
		noiseBits, err := capturer.CaptureNoise(capturingParams, requiredLength*2)
		if err != nil {
			return "", err
		}

		autocorr := math.Autocorrelation(noiseBits)
		for i, el := range autocorr {
			avg_autocorr[i] += el
		}
	}

	for i := 0; i < len(avg_autocorr); i++ {
		avg_autocorr[i] /= float64(numberOfCaptures)
	}

	if config.GetInstance().Debug.SaveAutocorrelation {
		// Print average autocorrelation to the file for debugging.
		f, _ := os.Create(config.GetInstance().Debug.SaveAutocorrelationFileName)
		f.WriteString("[")
		for _, el := range avg_autocorr {
			f.WriteString(fmt.Sprintf("%f,", el))
		}
		f.WriteString("]")
		f.Close()
	}

	// As a convention, let every template start with 1 (to not lose one bit).
	bitTemplate := "1"
	for i := 0; i < len(avg_autocorr)-1; i++ {
		if avg_autocorr[i] < avg_autocorr[i+1] {
			bitTemplate += "0"
		} else {
			bitTemplate += "1"
		}
	}

	return bitTemplate, nil
}
