package bittemplate

import (
	"testing"

	"github.com/gordonklaus/portaudio"

	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/devices"
)

func TestGenerateBitTemplate(t *testing.T) {
	portaudio.Initialize()
	defer portaudio.Terminate()

	inputDevices, err := devices.GetInputDevices()
	if err != nil {
		t.Fatalf(err.Error())
	}

	if len(inputDevices) == 0 {
		t.Fatalf("There are no available input devices")
	}

	testCases := [][]int{
		[]int{10, 0},
		[]int{1, 10},
		[]int{20, 100},
		[]int{10, 1000},
		[]int{0, 1},
	}

	// The most common capturing parameters.
	// To avoid the "Composite literal uses unkeyed fields" warning,
	// we have to explicitly specify all fields as "key:value".
	// More info on this:
	// https://stackoverflow.com/questions/49461354/go-vet-composite-literal-uses-unkeyed-fields-with-embedded-types
	capturingParams := capturing.NoiseCapturingParams{
		InputDevice:     inputDevices[0],
		SampleRate:      44100,
		FramesPerBuffer: 256,
		CapturingOffset: 500,
		NoiseBits:       []int{16},
	}

	for _, c := range testCases {
		template, err := GenerateBitTemplate(capturingParams, c[0], c[1])
		if err != nil {
			t.Errorf(err.Error())
		}

		if len(template) != c[1] {
			t.Errorf("Unexpected template length %d (expected %d)", len(template), c[1])
		}

		for _, el := range template {
			if el != '0' && el != '1' {
				t.Errorf("Unexpected template value %c", el)
			}
		}
	}
}
