package bittemplate

import (
	"errors"
)

// Checks if given string is a valid bit template (correct length and consists only of 0's and 1's).
func IsValidTemplate(template string, targetTemplateLength int) bool {
	valid := true

	if len(template) != targetTemplateLength {
		valid = false
	}

	for _, el := range template {
		if el != '0' && el != '1' {
			valid = false
			break
		}
	}

	return valid
}

// Returns the percentage of similarity between two bit templates.
func CompareTemplates(t1, t2 string, targetTemplateLength int) (float64, error) {
	if !IsValidTemplate(t1, targetTemplateLength) || !IsValidTemplate(t2, targetTemplateLength) {
		return -1, errors.New("Invalid template")
	}

	diffs := 0
	for i := 0; i < len(t1); i++ {
		if t1[i] != t2[i] {
			diffs++
		}
	}

	return 100 - (float64(diffs)/float64(targetTemplateLength))*100, nil
}
