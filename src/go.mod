module gitlab.com/kusarius/noise-self-identification

go 1.13

require (
	github.com/gen2brain/beeep v0.0.0-20200420150314-13046a26d502
	github.com/go-ole/go-ole v1.2.4
	github.com/gopherjs/gopherjs v0.0.0-20200217142428-fce0ec30dd00 // indirect
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/gotk3/gotk3 v0.4.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/moutend/go-wca v0.1.1
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
)
