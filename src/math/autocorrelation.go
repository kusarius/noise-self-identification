package math

// Calculates the mean (or average) of list of values
func Mean(x []float64) float64 {
	sum := 0.0
	for _, el := range x {
		sum += el
	}
	return sum / float64(len(x))
}

// Calculates the autocorrelation of list of values using naive algorithm.
// Complexity is O(n^2).
// More information: https://www.alanzucconi.com/2016/06/06/autocorrelation-function
func Autocorrelation(x []float64) []float64 {
	mn := Mean(x)
	res := make([]float64, len(x)/2)

	var n, d float64
	var xim float64

	for t := 0; t < len(res); t++ {
		n, d = 0.0, 0.0

		for i := 0; i < len(x); i++ {
			xim = x[i] - mn
			n += xim * (x[(i+t)%len(x)] - mn)
			d += xim * xim
		}

		res[t] = n / d
	}

	return res
}
