package math

import (
	"math/rand"
	"testing"
	"time"
)

func TestAutocorrelation(t *testing.T) {
	// Initialize the RNG.
	rand.Seed(time.Now().Unix())

	// Generate random series.
	data := make([]float64, 1000)
	for i := 0; i < 1000; i++ {
		// Numbers will be in a range of [-50; 50).
		data[i] = rand.Float64()*100 - 50
	}

	autocorr := Autocorrelation(data)
	if autocorr[0] != 1.0 {
		t.Errorf("First value of autocorrelation series must be 1.0, but got %f", autocorr[0])
	}

	if len(autocorr) != len(data)/2 {
		t.Errorf("Length of autocorrelation series must me %d, but the actual length is %d", len(data)/2, len(autocorr))
	}

	for i := 0; i < len(autocorr); i++ {
		if autocorr[i] < -1 || autocorr[i] > 1 {
			t.Errorf("Autocorrelation values must be in range of [-1; 1], but found a value %f", autocorr[i])
		}
	}
}
