package audiocontrol

import (
	"errors"
	"math"

	"github.com/go-ole/go-ole"
	"github.com/moutend/go-wca"
)

// Returns the current master volume (0 to 100).
func GetMasterVolume() (int, error) {
	vol, err := invokeAPI(func(aev *wca.IAudioEndpointVolume) (interface{}, error) {
		var level float32
		err := aev.GetMasterVolumeLevelScalar(&level)
		vol := int(math.Floor(float64(level*100.0 + 0.5)))
		return vol, err
	})

	if err != nil {
		return -1, err
	}

	return vol.(int), err
}

// Sets the master volume to the specified value.
func SetMasterVolume(volume int) error {
	if volume < 0 || volume > 100 {
		return errors.New("Out of valid volume range")
	}

	_, err := invokeAPI(func(aev *wca.IAudioEndpointVolume) (interface{}, error) {
		err := aev.SetMasterVolumeLevelScalar(float32(volume)/100, nil)
		return nil, err
	})

	return err
}

// Returns the current mute status.
func IsMuted() (bool, error) {
	muted, err := invokeAPI(func(aev *wca.IAudioEndpointVolume) (interface{}, error) {
		var muted bool
		err := aev.GetMute(&muted)
		return muted, err
	})

	if err != nil {
		return false, err
	}

	return muted.(bool), err
}

// Mutes the audio.
func Mute() error {
	_, err := invokeAPI(func(aev *wca.IAudioEndpointVolume) (interface{}, error) {
		err := aev.SetMute(true, nil)
		return nil, err
	})

	return err
}

// Unmutes the audio.
func Unmute() error {
	_, err := invokeAPI(func(aev *wca.IAudioEndpointVolume) (interface{}, error) {
		err := aev.SetMute(false, nil)
		return nil, err
	})

	return err
}

// Common function for calling the Windows Core Audio API.
func invokeAPI(f func(aev *wca.IAudioEndpointVolume) (interface{}, error)) (ret interface{}, err error) {
	// We will call CoUninitialize only in case we have a successfull call to CoInitializeEx.
	// Unsuccessful call to CoInitializeEx can probably mean that COM library is already
	// initialized in the current thread.
	if err = ole.CoInitializeEx(0, ole.COINIT_APARTMENTTHREADED); err == nil {
		defer ole.CoUninitialize()
	}

	var mmde *wca.IMMDeviceEnumerator
	if err = wca.CoCreateInstance(wca.CLSID_MMDeviceEnumerator, 0, wca.CLSCTX_ALL, wca.IID_IMMDeviceEnumerator, &mmde); err != nil {
		return
	}
	defer mmde.Release()

	var mmd *wca.IMMDevice
	if err = mmde.GetDefaultAudioEndpoint(wca.ERender, wca.EConsole, &mmd); err != nil {
		return
	}
	defer mmd.Release()

	var aev *wca.IAudioEndpointVolume
	if err = mmd.Activate(wca.IID_IAudioEndpointVolume, wca.CLSCTX_ALL, nil, &aev); err != nil {
		return
	}
	defer aev.Release()

	ret, err = f(aev)
	return
}
