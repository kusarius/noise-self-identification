package audiocontrol

// This function checks the state of mute and mutes/unmutes accordingly.
// This is needed to avoid possible muting the already muted device,
// and the same with unmute.
func SafeMuteUnmute(mute bool) error {
	isMuted, err := IsMuted()
	if err != nil {
		return err
	}

	if !isMuted && mute {
		err = Mute()
	} else if isMuted && !mute {
		err = Unmute()
	}

	return err
}
