package audiocontrol

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

var (
	useAmixer     = false
	volumePattern = regexp.MustCompile(`\d+%`)
)

func init() {
	if _, err := exec.LookPath("pactl"); err != nil {
		useAmixer = true
	}
}

// Returns the current master volume (0 to 100).
func GetMasterVolume() (int, error) {
	out, err := execCmd(getVolumeCmd())
	if err != nil {
		return -1, err
	}

	return parseVolume(string(out))
}

// Sets the master volume to the specified value.
func SetMasterVolume(volume int) error {
	if volume < 0 || volume > 100 {
		return errors.New("Out of valid volume range")
	}

	_, err := execCmd(setVolumeCmd(volume))
	return err
}

// Returns the current mute status.
func IsMuted() (bool, error) {
	out, err := execCmd(isMutedCmd())
	if err != nil {
		return false, err
	}

	return parseMuted(string(out))
}

// Mutes the audio.
func Mute() error {
	_, err := execCmd(muteCmd())
	return err
}

// Unmutes the audio.
func Unmute() error {
	_, err := execCmd(unmuteCmd())
	return err
}

func execCmd(cmdArgs []string) ([]byte, error) {
	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	cmd.Env = append(os.Environ(), "LANG=C", "LC_ALL=C")

	out, err := cmd.Output()
	if err != nil {
		err = errors.New(fmt.Sprintf("Failed to execute \"%s\": %s", strings.Join(cmdArgs, " "), err.Error()))
	}

	return out, err
}

func getVolumeCmd() []string {
	if useAmixer {
		return []string{"amixer", "get", "Master"}
	}

	return []string{"pactl", "list", "sinks"}
}

func setVolumeCmd(volume int) []string {
	if useAmixer {
		return []string{"amixer", "set", "Master", strconv.Itoa(volume) + "%"}
	}

	return []string{"pactl", "set-sink-volume", "0", strconv.Itoa(volume) + "%"}
}

func isMutedCmd() []string {
	if useAmixer {
		return []string{"amixer", "get", "Master"}
	}

	return []string{"pactl", "list", "sinks"}
}

func muteCmd() []string {
	if useAmixer {
		return []string{"amixer", "-D", "pulse", "set", "Master", "mute"}
	}

	return []string{"pactl", "set-sink-mute", "0", "1"}
}

func unmuteCmd() []string {
	if useAmixer {
		return []string{"amixer", "-D", "pulse", "set", "Master", "unmute"}
	}

	return []string{"pactl", "set-sink-mute", "0", "0"}
}

func parseVolume(out string) (int, error) {
	lines := strings.Split(out, "\n")
	for _, line := range lines {
		s := strings.TrimLeft(line, " \t")
		if (useAmixer && strings.Contains(s, "Playback") && strings.Contains(s, "%")) ||
			(!useAmixer && strings.HasPrefix(s, "Volume:")) {
			volumeStr := volumePattern.FindString(s)
			return strconv.Atoi(volumeStr[:len(volumeStr)-1])
		}
	}

	return -1, errors.New("No volume found")
}

func parseMuted(out string) (bool, error) {
	lines := strings.Split(out, "\n")
	for _, line := range lines {
		s := strings.TrimLeft(line, " \t")
		if (useAmixer && strings.Contains(s, "Playback") && strings.Contains(s, "%")) ||
			(!useAmixer && strings.HasPrefix(s, "Mute: ")) {
			if strings.Contains(s, "[off]") || strings.Contains(s, "yes") {
				return true, nil
			} else if strings.Contains(s, "[on]") || strings.Contains(s, "no") {
				return false, nil
			}
		}
	}

	return false, errors.New("No muted information found")
}
