package audiocontrol

import (
	"testing"
)

func TestGetMasterVolume(t *testing.T) {
	_, err := GetMasterVolume()
	if err != nil {
		t.Errorf("Get master volume failed: %s", err.Error())
	}
}

func TestSetMasterVolume(t *testing.T) {
	for _, vol := range []int{0, 37, 54, 20, 100} {
		err := SetMasterVolume(vol)
		if err != nil {
			t.Errorf("Set master volume failed: %s", err.Error())
		}

		newVol, err := GetMasterVolume()
		if err != nil {
			t.Errorf("Get master volume failed: %s", err.Error())
		}

		if vol != newVol {
			t.Errorf("Set master volume failed: expected: %d, but got %d", newVol, vol)
		}
	}
}

func TestMute(t *testing.T) {
	err := Mute()
	if err != nil {
		t.Errorf("Mute failed: %s", err.Error())
	}

	muted, err := IsMuted()
	if err != nil {
		t.Errorf("Is muted failed: %s", err.Error())
	}

	if !muted {
		t.Errorf("Mute failed: expected to be muted, but actually isn't")
	}
}

func TestUnmute(t *testing.T) {
	err := Unmute()
	if err != nil {
		t.Errorf("Unmute failed: %s", err.Error())
	}

	muted, err := IsMuted()
	if err != nil {
		t.Errorf("Is muted failed: %s", err.Error())
	}

	if muted {
		t.Errorf("Unmute failed: expected to be unmuted, but actually isn't")
	}
}
