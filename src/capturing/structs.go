package capturing

import (
	"github.com/gordonklaus/portaudio"
)

// Noise capturing parameters:
// inputDevice:     Device to capture noise from.
// sampleRate:      Capturing sample rate.
// framesPerBuffer: Number of samples in each capturing buffer.
// capturingOffset: Number of samples to skip before capturing noise.
// noiseBits:       Position of bits in a sample (0 - least significant, 31 - most significant) that counts as noise.
type NoiseCapturingParams struct {
	InputDevice     *portaudio.DeviceInfo
	SampleRate      int
	FramesPerBuffer int
	CapturingOffset int
	NoiseBits       []int
}

type NoiseCapturer interface {
	CaptureNoise(capturingParams NoiseCapturingParams, requiredLength int) ([]float64, error)
}
