package raw

import (
	"testing"

	"github.com/gordonklaus/portaudio"
	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/devices"
)

func TestRawCapturer(t *testing.T) {
	portaudio.Initialize()
	defer portaudio.Terminate()

	inputDevices, err := devices.GetInputDevices()
	if err != nil {
		t.Fatalf(err.Error())
	}

	if len(inputDevices) == 0 {
		t.Fatalf("There are no available input devices")
	}

	testCases := []capturing.NoiseCapturingParams{
		{inputDevices[0], 44100, 64, 0, []int{0, 5}},
		{inputDevices[0], 44100, 128, 100, []int{10, 11, 12}},
		{inputDevices[0], 44100, 256, 200, []int{31, 0}},
		{inputDevices[0], 48000, 512, 500, []int{15}},
		{inputDevices[0], 48000, 1024, 1000, []int{2, 0, 0, 20, 11}},
	}

	requiredSamples := 1000
	capturer := new(NoiseCapturer)

	for _, params := range testCases {
		noise, err := capturer.CaptureNoise(params, requiredSamples)
		if err != nil {
			t.Errorf(err.Error())
		}

		if len(noise) != requiredSamples {
			t.Errorf("Unexpected noise length %d (expected %d)", len(noise), requiredSamples)
		}
	}
}
