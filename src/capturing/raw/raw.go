package raw

import (
	"errors"
	"fmt"
	"os"

	"github.com/gordonklaus/portaudio"
	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/config"
)

type NoiseCapturer struct{}

// Captures the given amount of raw samples from input device.
// capturingParams.NoiseBits is ignored.
// If any error occurs during capturing, it will be returned to the client.
func (c *NoiseCapturer) CaptureNoise(capturingParams capturing.NoiseCapturingParams, requiredLength int) ([]float64, error) {
	params := portaudio.HighLatencyParameters(capturingParams.InputDevice, nil)
	params.Input.Channels = 1
	params.SampleRate = float64(capturingParams.SampleRate)

	audioInput := make([]int32, capturingParams.FramesPerBuffer)
	stream, err := portaudio.OpenStream(params, audioInput)
	if err != nil {
		return nil, errors.New("Could not create input stream! Error: " + err.Error())
	}

	err = stream.Start()
	if err != nil {
		return nil, errors.New("Could not start input stream! Error: " + err.Error())
	}

	defer stream.Close()
	defer stream.Stop()

	currentNoiseIndex := 0

	noiseSamples := make([]float64, requiredLength+capturingParams.CapturingOffset)

	var noiseDebugFile *os.File = nil
	saveNoise := false

	if config.GetInstance().Debug.SaveNoise {
		noiseDebugFile, err = os.OpenFile(config.GetInstance().Debug.SaveNoiseFileName, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return nil, errors.New("Could not open the file to save noise! Error: " + err.Error())
		}

		saveNoise = true
		defer noiseDebugFile.Close()
	}

	for currentNoiseIndex < requiredLength+capturingParams.CapturingOffset {
		err = stream.Read()
		if err != nil {
			return nil, errors.New("Could not read from input stream! Error: " + err.Error())
		}

		for _, sample := range audioInput {
			if currentNoiseIndex >= requiredLength+capturingParams.CapturingOffset {
				break
			}

			if saveNoise {
				noiseDebugFile.Write([]byte(fmt.Sprintf("%032b\n", sample)))
			}

			noiseSamples[currentNoiseIndex] = float64(sample)
			currentNoiseIndex++
		}
	}

	return noiseSamples[capturingParams.CapturingOffset : capturingParams.CapturingOffset+requiredLength], nil
}
