package selective

import (
	"errors"
	"fmt"
	"os"

	"github.com/gordonklaus/portaudio"
	"gitlab.com/kusarius/noise-self-identification/capturing"
	"gitlab.com/kusarius/noise-self-identification/config"
)

type NoiseCapturer struct{}

// Captures the given amount of samples from input device, then extracts the noise bit from each sample.
// Returns list of bits as []float64 for more convenient further processing.
// If any error occurs during capturing, it will be returned to the client.
func (c *NoiseCapturer) CaptureNoise(capturingParams capturing.NoiseCapturingParams, requiredLength int) ([]float64, error) {
	params := portaudio.HighLatencyParameters(capturingParams.InputDevice, nil)
	params.Input.Channels = 1
	params.SampleRate = float64(capturingParams.SampleRate)

	audioInput := make([]int32, capturingParams.FramesPerBuffer)
	stream, err := portaudio.OpenStream(params, audioInput)
	if err != nil {
		return nil, errors.New("Could not create input stream! Error: " + err.Error())
	}

	err = stream.Start()
	if err != nil {
		return nil, errors.New("Could not start input stream! Error: " + err.Error())
	}

	currentNoiseIndex := 0

	// Add len(capturingParams.NoiseBits) to ensure that we won't go out of bounds.
	noiseBits := make([]float64, requiredLength+capturingParams.CapturingOffset+len(capturingParams.NoiseBits))

	var noiseDebugFile *os.File = nil
	saveNoise := false // Helper flag to not access the config on each iteration.

	if config.GetInstance().Debug.SaveNoise {
		noiseDebugFile, err = os.OpenFile(config.GetInstance().Debug.SaveNoiseFileName, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return nil, errors.New("Could not open the file to save noise! Error: " + err.Error())
		}

		saveNoise = true
		defer noiseDebugFile.Close()
	}

	for currentNoiseIndex < requiredLength+capturingParams.CapturingOffset {
		err = stream.Read()
		if err != nil {
			stream.Stop()
			stream.Close()

			return nil, errors.New("Could not read from input stream! Error: " + err.Error())
		}

		for _, sample := range audioInput {
			if currentNoiseIndex >= requiredLength+capturingParams.CapturingOffset {
				break
			}

			if sample < 0 {
				sample = -sample
			}

			if saveNoise {
				noiseDebugFile.Write([]byte(fmt.Sprintf("%032b\n", sample)))
			}

			for _, nb := range capturingParams.NoiseBits {
				noiseBits[currentNoiseIndex] = float64((sample & (1 << uint(nb))) >> uint(nb))
				currentNoiseIndex++
			}
		}
	}

	stream.Stop()
	stream.Close()

	return noiseBits[capturingParams.CapturingOffset : capturingParams.CapturingOffset+requiredLength], nil
}
