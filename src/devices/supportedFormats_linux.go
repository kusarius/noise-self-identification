package devices

/*
#cgo LDFLAGS: -lasound

#include <string.h>
#include <alsa/asoundlib.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static const unsigned int rates[] = { 8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000, 96000 };

static const int formatsBitDepth[] = { 8, 8, 16, 16, 24, 24, 32, 32 };
static const snd_pcm_format_t formats[] = {
    SND_PCM_FORMAT_S8,
    SND_PCM_FORMAT_U8,
    SND_PCM_FORMAT_S16,
    SND_PCM_FORMAT_U16,
    SND_PCM_FORMAT_S24,
    SND_PCM_FORMAT_U24,
    SND_PCM_FORMAT_S32,
    SND_PCM_FORMAT_U32
};

snd_pcm_t *pcm = NULL;
snd_pcm_hw_params_t *hw_params = NULL;

int initDevice(char* deviceName) {
    int err;

    err = snd_pcm_hw_params_malloc(&hw_params);
    if (err < 0) {
        return 1;
    }

    err = snd_pcm_open(&pcm, deviceName, SND_PCM_STREAM_CAPTURE, SND_PCM_ASYNC);
    if (err < 0) {
        return 1;
    }

    err = snd_pcm_hw_params_any(pcm, hw_params);
    if (err < 0) {
        return 1;
    }

    return 0;
}

char* getSupportedRates() {
    char* res = malloc(sizeof(char) * 200);
    memset(res, 0, 200);

    for (unsigned int i = 0; i < ARRAY_SIZE(rates); ++i) {
        if (!snd_pcm_hw_params_test_rate(pcm, hw_params, rates[i], 0)) {
            char buffer[10];
            sprintf(buffer, "%u ", rates[i]);
            strcat(res, buffer);
        }
    }

    return res;
}

char* getSupportedBitDepths() {
    char* res = malloc(sizeof(char) * 200);
    memset(res, 0, 200);

    for (unsigned int i = 0; i < ARRAY_SIZE(formats); ++i) {
        if (!snd_pcm_hw_params_test_format(pcm, hw_params, formats[i])) {
            char buffer[10];
            sprintf(buffer, "%u ", formatsBitDepth[i]);
            strcat(res, buffer);
        }
    }

    return res;
}
*/
import "C"

import (
	"github.com/gordonklaus/portaudio"
	"strconv"
	"strings"
)

// Returns the list of supported formats of specified device (Linux).
func GetDeviceSupportedFormats(device *portaudio.DeviceInfo) SupportedAudioFormats {
	supportedFormats := SupportedAudioFormats{}

	if C.initDevice(C.CString(device.Name)) == 0 {
		rates, depths := C.GoString(C.getSupportedRates()), C.GoString(C.getSupportedBitDepths())

		for _, rate := range strings.Split(rates, " ") {
			rateNum, err := strconv.Atoi(rate)
			if err == nil {
				supportedFormats.SampleRates = append(supportedFormats.SampleRates, rateNum)
			}
		}

		for _, depth := range strings.Split(depths, " ") {
			depthNum, err := strconv.Atoi(depth)
			if err == nil {
				supportedFormats.BitDepths = append(supportedFormats.BitDepths, depthNum)
			}
		}
	}

	return supportedFormats
}
