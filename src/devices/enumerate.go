package devices

import (
	"errors"
	"runtime"

	"github.com/gordonklaus/portaudio"
)

// Returns primary and secondary host API for the current system.
// If operating system is not supported - returns error to the client.
func getAPIs() (portaudio.HostApiType, portaudio.HostApiType, error) {
	var primaryAPI, secondaryAPI portaudio.HostApiType

	switch runtime.GOOS {
	case "windows":
		primaryAPI = portaudio.MME
		secondaryAPI = portaudio.WASAPI
	case "linux":
		primaryAPI = portaudio.ALSA
		secondaryAPI = portaudio.OSS
	default:
		return -1, -1, errors.New("System is not supported!")
	}

	return primaryAPI, secondaryAPI, nil
}

// Returns the list of available input devices according to the primary and secondary host APIs.
// If any error occurs, it will be returned to the client.
func GetInputDevices() ([]*portaudio.DeviceInfo, error) {
	primaryAPI, secondaryAPI, err := getAPIs()
	if err != nil {
		return nil, errors.New("Could not get primary and secondary host API! Error: " + err.Error())
	}

	devices, err := portaudio.Devices()
	if err != nil {
		return nil, errors.New("Could not get list of available devices! Error: " + err.Error())
	}

	inputDevices := make([]*portaudio.DeviceInfo, 0)

	// First, add all devices with primary host API.
	for _, dv := range devices {
		if dv.MaxInputChannels != 0 && (dv.HostApi.Type == primaryAPI) {
			inputDevices = append(inputDevices, dv)
		}
	}

	// And then add devices with secondary host API.
	for _, dv := range devices {
		if dv.MaxInputChannels != 0 && (dv.HostApi.Type == secondaryAPI) {
			inputDevices = append(inputDevices, dv)
		}
	}

	return inputDevices, nil
}
