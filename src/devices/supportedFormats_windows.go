package devices

/*
#cgo LDFLAGS: -lwinmm

#include <windows.h>
#include <mmsystem.h>

DWORD getDeviceFormats(char* deviceName) {
    UINT inputsCount = waveInGetNumDevs();
    UINT outputsCount = waveOutGetNumDevs();

    for (UINT dev = 0; dev < inputsCount; dev++) {
        WAVEINCAPS caps = {};
        MMRESULT mmr = waveInGetDevCaps(dev, &caps, sizeof(caps));

        if (mmr == MMSYSERR_NOERROR && strcmp(deviceName, caps.szPname) == 0) {
            return caps.dwFormats;
        }
    }

    for (UINT dev = 0; dev < outputsCount; dev++) {
        WAVEOUTCAPS caps = {};
        MMRESULT mmr = waveOutGetDevCaps(dev, &caps, sizeof(caps));

        if (mmr == MMSYSERR_NOERROR && strcmp(deviceName, caps.szPname) == 0) {
            return caps.dwFormats;
        }
    }

    return 0;
}
*/
import "C"

import (
	"github.com/gordonklaus/portaudio"
)

var formatMapping = map[int][]int{
	C.WAVE_FORMAT_1M08: []int{11025, 8},
	C.WAVE_FORMAT_1S08: []int{11025, 8},
	C.WAVE_FORMAT_1M16: []int{11025, 16},
	C.WAVE_FORMAT_1S16: []int{11025, 16},

	C.WAVE_FORMAT_2M08: []int{22050, 8},
	C.WAVE_FORMAT_2S08: []int{22050, 8},
	C.WAVE_FORMAT_2M16: []int{22050, 16},
	C.WAVE_FORMAT_2S16: []int{22050, 16},

	C.WAVE_FORMAT_44M08: []int{44100, 8},
	C.WAVE_FORMAT_44S08: []int{44100, 8},
	C.WAVE_FORMAT_44M16: []int{44100, 16},
	C.WAVE_FORMAT_44S16: []int{44100, 16},

	C.WAVE_FORMAT_48M08: []int{48000, 8},
	C.WAVE_FORMAT_48S08: []int{48000, 8},
	C.WAVE_FORMAT_48M16: []int{48000, 16},
	C.WAVE_FORMAT_48S16: []int{48000, 16},

	C.WAVE_FORMAT_96M08: []int{96000, 8},
	C.WAVE_FORMAT_96S08: []int{96000, 8},
	C.WAVE_FORMAT_96M16: []int{96000, 16},
	C.WAVE_FORMAT_96S16: []int{96000, 16},
}

// Returns the list of supported formats of specified device (Windows).
func GetDeviceSupportedFormats(device *portaudio.DeviceInfo) SupportedAudioFormats {
	deviceFormats := uint32(C.getDeviceFormats(C.CString(device.Name)))
	supportedFormats := SupportedAudioFormats{}

	for k, v := range formatMapping {
		if deviceFormats&uint32(k) != 0 {
			rate, depth := v[0], v[1]

			present := false
			for _, el := range supportedFormats.SampleRates {
				if el == rate {
					present = true
					break
				}
			}

			if !present {
				supportedFormats.SampleRates = append(supportedFormats.SampleRates, rate)
			}

			present = false
			for _, el := range supportedFormats.BitDepths {
				if el == depth {
					present = true
					break
				}
			}

			if !present {
				supportedFormats.BitDepths = append(supportedFormats.BitDepths, depth)
			}
		}
	}

	return supportedFormats
}
