package devices

// Supported sample rates and bit depths by audio device.
type SupportedAudioFormats struct {
	SampleRates, BitDepths []int
}
