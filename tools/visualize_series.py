import sys
import matplotlib.pyplot as plt

if len(sys.argv) < 2:
    print('Usage: python ' + sys.argv[0] + ' <filename>')
    exit()

for file in sys.argv[1:]:
    with open(file) as f:
        # I know, this is very unsafe, but convenient for quick visualizations.
        series = eval(f.read())
        x = list(range(len(series)))

        plt.plot(x, series)

plt.show()