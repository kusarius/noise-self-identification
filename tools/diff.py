def diff(x, y):
    '''Returns the number of non-equal elements in two lists'''

    if len(x) != len(y):
        return None

    res = 0
    for i in range(len(x)):
        if x[i] != y[i]:
            res += 1

    return res
