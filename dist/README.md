### How to test (Windows):
1.  Open the terminal
2.  `cd` to the directory with `nsi.exe`
3.  Run the following command: `nsi.exe --action=generate --template=myTemplate.tpl`
4.  You will see the list of available audio devices on your computer
5.  Select any device that has `Host API: MME` in properties
6.  Your computer will be **muted** for a couple of seconds
7.  When the program finishes, run the command: `nsi.exe --action=validate --template=myTemplate.tpl`
8.  Select the **same** device you have checked in a previous steps
9.  Again, your computer will be **muted** for some time
10. After execution, you should see the message `Confidence: ...` at the end of program's output
11. **Ensure** that the confidence level is **high** (>80%)
12. Run the following command: `nsi.exe --action=validate --template=testTemplate.tpl`
13. Select the **same** device you have checked in a previous steps
14. **Ensure** that the confidence level is **low** (~50%)